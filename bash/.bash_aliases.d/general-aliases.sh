## some more ls aliases
# List aliases
alias l='ls -cCF'
alias ll='ls -l'
alias la='ls -A'
alias lg='ls -cCF  | grep '

##Compass watch
alias cow='compass watch -e development --trace'
alias coc='compass compile -e production --force'
